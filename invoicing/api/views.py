from rest_framework import views, response, status
from invoice.models import Invoice
from django.shortcuts import get_object_or_404
from rest_framework import permissions
from .serializers import OrderSerializer, ProductSerializer, CreateProfileSerializer, InvoiceSerializer, ProfileSerializer
from rest_framework.exceptions import PermissionDenied
from company.models import Profile, Company
from products.models import Product
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication


class InvoiceListView(views.APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self,request, format=None):
        invoices = Invoice.objects.all()
        try:
            len(invoices)
            many=True
        except TypeError:
            many=False
        serializer = InvoiceSerializer(invoices, many=many)
        return response.Response(serializer.data)


class InvoiceView(views.APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)
    def get(self, request, id):
        invoice = get_object_or_404(Invoice, pk=id)
        if invoice.issuer == request.user.profile.company or invoice.tax_id == request.user.profile.company.tax_id:
            serializer = InvoiceSerializer(invoice, many=False)
            return response.Response(serializer.data)
        else:
            raise PermissionDenied()

class UsersView(views.APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication,)
    permission_classes= (permissions.IsAuthenticated,)

    def get(self,request):
        users = Profile.objects.filter(company=request.user.profile.company)
        try:
            len(users)
            many=True
        except TypeError:
            many=False
        serializer = ProfileSerializer(users, many=many)
        return response.Response(serializer.data)


class CreateUserView(CreateAPIView):
    serializer_class = CreateProfileSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def create(self, request,  *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            validated_data = serializer.validated_data
            serializer.create(validated_data, request.user.profile.company)
            return Response(serializer.validated_data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ListProductsView(views.APIView):

    def get(self, request, company_id):
        company = get_object_or_404(Company, pk=company_id)
        products = Product.objects.filter(company=company)
        serializer = ProductSerializer(instance=products, many=True)
        return response.Response(serializer.data)

class MakeOrderView(CreateAPIView):
    serializer_class = OrderSerializer

