from rest_framework import serializers
from invoice.models import Invoice
from django.contrib.auth.models import User
from company.models import Profile, Company, TemporaryAddress
from geographical.models import Country
from financial.models import Currency
from products.models import Product, ProductCategory
from orders.models import Order, OrderStatus, Item
from django.db import transaction
from django.contrib.auth.hashers import make_password

class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = TemporaryAddress
        exclude = []

class CompanySerializer(serializers.ModelSerializer):
    headquarters = AddressSerializer(required=False)
    class Meta:
        model = Company
        exclude = ['owner', 'customers']

class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        exclude = []

class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        exclude = []

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        exclude =["is_staff"]


class ProtectedUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']



class ProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=True)
    class Meta:
        model = Profile
        exclude= []

    def create(self, validated_data, company):
        user_data = validated_data.pop('user')
        user = UserSerializer.create(UserSerializer, validated_data=user_data)

        profile, created = Profile.objects.update_or_create(
            user=user, company=company, role=validated_data.pop('role')
            )
        return profile

class InvoiceSerializer(serializers.ModelSerializer):
    #created_by = ProtectedUserSerializer(required=True)
    issuer = CompanySerializer(required=True)
    #currency = CurrencySerializer(required=True)

    class Meta:
        model = Invoice
        exclude = []

class ProductCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductCategory
        exclude = ['company']

class ProductSerializer(serializers.ModelSerializer):
    category = ProductCategorySerializer(required=True)
    class Meta:
        model = Product
        exclude = ['company']

class CreateProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=True)
    employment_date = serializers.DateField(required=False)
    role = serializers.ChoiceField(choices=Profile.ROLE_CHOICES)
    class Meta:
        model = Profile
        exclude = ['company']
    def create(self, validated_data, company):
        with transaction.atomic():
            sid = transaction.savepoint()
            try:
                user_data  = validated_data.pop('user')
                user_data['password'] = make_password(user_data['password'])
                user = User.objects.create(**user_data)
                profile = Profile.objects.get(user=user)
                user.save()
                for key,value in validated_data.items():
                    setattr(profile, key, value)
                profile.company = company
                profile.save()

                validated_data['user'] = user_data

            except Exception as e:
                transaction.rollback(sid)
        transaction.commit()
        return validated_data



class ItemSerializer(serializers.ModelSerializer):
    item_id = serializers.IntegerField()
    class Meta:
        model = Item
        fields = "__all__"


class OrderSerializer(serializers.ModelSerializer):
    items = ItemSerializer(many=True)
    delivery_address = AddressSerializer(required=True)

    class Meta:
        model = Order
        exclude = ['order_date']

    def create(self, validated_data):
        items_data = validated_data['items']
        address_data = validated_data['delivery_address']
        company = validated_data['company']
        tax_id = validated_data['tax_id']
        with transaction.atomic():
            sid = transaction.savepoint()
            try:
                address = TemporaryAddress.objects.create(**address_data)
                address.save()
                order = Order.objects.create(company=company,items=items_data,delivery_address=address, tax_id=tax_id)
                order.save()
                status = OrderStatus.objects.create(order=order, assigned_to=None, status=OrderStatus.PENDING)
                status.save()
            except:
                transaction.rollback(sid)
        return validated_data


class OrderStatusSerializer(serializers.ModelSerializer):
    order = OrderSerializer(required=True)
    class Meta:
        model = OrderStatus

