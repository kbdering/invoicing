from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .views import MakeOrderView, InvoiceView, InvoiceListView, UsersView, CreateUserView, ListProductsView
from rest_framework.authtoken import views
urlpatterns = [
  url(r'^invoice/$', InvoiceListView.as_view()),
  url(r'invoice/(?P<id>\d+)/$', InvoiceView.as_view()),
  url(r'users/$', UsersView.as_view()),
  url(r'users/create$', CreateUserView.as_view()),
  url(r'company/(?P<company_id>\d+)/products$', ListProductsView.as_view()),
  url(r'orders/create', MakeOrderView.as_view()),
]

urlpatterns += [
    url(r'^api-token-auth/', views.obtain_auth_token)
]
urlpatterns = format_suffix_patterns(urlpatterns)