from django.db import models
from financial.models import Currency

from django.contrib import admin


class CurrencyRelationship(models.Model):
    country = models.ForeignKey('Country', on_delete=models.CASCADE)
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE)


class Country(models.Model):
    name = models.CharField(max_length=150, unique=True)
    currencies = models.ManyToManyField(Currency, symmetrical=False, through=CurrencyRelationship)
    name_abbreviation = models.CharField(max_length=20, null=True)
    cache_key = "countries_set"

    def __str__(self):
        return self.name


admin.site.register(CurrencyRelationship)
admin.site.register(Country)
