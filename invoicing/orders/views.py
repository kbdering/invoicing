from django.shortcuts import render
from django.views.generic import View
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.shortcuts import get_object_or_404, Http404, redirect
from django.contrib import messages
from django.db import transaction
from orders.models import Order, OrderStatus

class OrdersListView(View):
    @method_decorator(login_required)
    def get(self, request):
        company = request.user.profile.company
        orders_list = Order.objects.select_related().filter(company=company, orderstatus__status=OrderStatus.PENDING)
        assigned_orders = Order.objects.select_related().filter(orderstatus__status=OrderStatus.ACCEPTED, orderstatus__assigned_to=request.user)
        return render(request,template_name="orders/list.html", context={'orders': orders_list, "assigned_orders" : assigned_orders})
class OrderView(View):
    @method_decorator(login_required)
    def get(self, request, order_id):
        order = Order.objects.select_related("orderstatus", "delivery_address").get(pk=order_id)
        if not order:
            raise Http404("the order does not exist")
        if order.company != request.user.profile.company:
            raise Http404("Order does not belong to your company")
        return render(request, template_name="orders/order.html", context={"order": order})

class AcceptOrderView(View):
    @method_decorator(login_required)
    def post(self, request, order_id):
        order = get_object_or_404(Order, pk=order_id)
        if order.company != request.user.profile.company:
            raise Http404

        order_status = OrderStatus.objects.get(order=order)
        if order_status.status == OrderStatus.PENDING:
            with transaction.atomic():
                sid = transaction.savepoint()
                try:
                    order_status.assigned_to = request.user
                    order_status.status = OrderStatus.ACCEPTED
                    order_status.save()
                except:
                    transaction.rollback(sid)
            transaction.commit()
            messages.add_message(request, messages.SUCCESS, "Order accepted")
            return redirect("list_orders")
        else:
            raise Http404

class CompleteOrderView(View):
    @method_decorator(login_required)
    def post(self, request, order_id):
        order = get_object_or_404(Order, pk=order_id)
        if order.company != request.user.profile.company:
            raise Http404("you cannot complete the order that wasn't assigned to you")

        order_status = OrderStatus.objects.get(order=order)
        if order_status.status == OrderStatus.ACCEPTED and order_status.assigned_to == request.user:
            with transaction.atomic():
                sid = transaction.savepoint()
                try:
                    order_status.status = OrderStatus.COMPLETED
                    order_status.save()
                except:
                    transaction.rollback(sid)
            transaction.commit()
            messages.add_message(request, messages.SUCCESS, "Order completed!")
            return redirect("list_orders")
        else:
            raise Http404
