from django.db import models
from django.contrib import admin
from company.models import Company, TemporaryAddress
from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField
from products.models import Product

class Order(models.Model):
    items = JSONField()
    order_date = models.DateTimeField(auto_now=True)
    delivery_address = models.OneToOneField(TemporaryAddress, on_delete=models.CASCADE)
    tax_id = models.CharField(max_length=50)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    class Meta:
        models.Index(fields=['items',])
        ordering = ['id']
class OrderStatus(models.Model):
    PENDING = "PENDING"
    ACCEPTED = "ACCEPTED"
    COMPLETED = "COMPLETED"
    STATUS_CHOICES = (
        (PENDING, "Pending"),
        (ACCEPTED, "Accepted"),
        (COMPLETED, "Completed")
    )
    order = models.OneToOneField(Order, on_delete=models.CASCADE)
    status = models.CharField(
        max_length=10,
        choices=STATUS_CHOICES,
        default=PENDING
    )
    assigned_to = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

class Item(models.Model):
    item_id = models.ForeignKey(Product, on_delete=models.PROTECT)
    amount = models.FloatField()
