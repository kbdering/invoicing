from django.urls import path
from django.urls import include
from .views import OrdersListView, OrderView, AcceptOrderView, CompleteOrderView
urlpatterns = [
    path("", OrdersListView.as_view(), name="list_orders"),
    path("<int:order_id>/", OrderView.as_view(), name="order_view"),
    path("<int:order_id>/accept", AcceptOrderView.as_view(), name="accept_order"),
    path("<int:order_id>/complete", CompleteOrderView.as_view(), name="complete_order")
]