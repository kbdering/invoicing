from django.core.cache import cache

def cache_per_param(ttl=None, param=None, cache_per_user=False, prefix=None, cache_post=False):
    def decorator(function):
        def apply_cache(request, *args, **kwargs):
            if cache_per_user:
                if request.user.is_anonymous:
                    user = 'anonymous'
                else:
                    user = request.user.id
            else:
                user = 'anonymous'
            if param:
                request_params = getattr(request, request.method)
                try:
                    cached_param = request_params[param]
                except KeyError:
                    cached_param = None


            if prefix:
                CACHE_KEY = '%s_%s_%s_%s'%(prefix, user, param, cached_param)
            else:
                CACHE_KEY = 'view_cache_%s_%s_%s_%s'%(function.__name__, user, param, cached_param)

            if not cache_post and request.method == 'POST':
                can_cache = False
            else:
                can_cache = True

            if can_cache:
                response = cache.get(CACHE_KEY, None)
            else:
                response = None

            if not response:
                response = function(request, *args, **kwargs)
                if can_cache:
                    cache.set(CACHE_KEY, response, ttl)
            return response
        return apply_cache
    return decorator