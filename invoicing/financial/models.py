from django.db import models
from django.contrib import admin


class TaxIdValidator(models.Model):
    regex = models.CharField(max_length=100)
    sample = models.CharField(max_length=100)


class TaxIdentifierType(models.Model):
    name = models.CharField(max_length=50)
    validator = models.ForeignKey(TaxIdValidator, on_delete=models.DO_NOTHING)


class TaxIdentifier(models.Model):
    name = models.CharField(max_length=150)
    type = models.ForeignKey(TaxIdentifierType, on_delete=models.DO_NOTHING)


class Currency(models.Model):
    name = models.CharField(max_length=45)
    symbol = models.CharField(max_length=3, null=True)
    alpha3 = models.CharField(max_length=3)

    def __str__(self):
        return self.name + " ({0})".format(self.alpha3)

    def __unicode__(self):
        return self.name + " ({0})".format(self.alpha3)


admin.site.register(Currency)