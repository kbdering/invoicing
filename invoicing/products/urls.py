from django.urls import path
from .views import AddProductView, ProductCategoryEditView, ProductCategoryView, ProductsListView
urlpatterns = [
    path("category/", ProductCategoryView.as_view(), name="product_category"),
    path("category/add", ProductCategoryEditView.as_view(), name="add_product_category"),
    path("", ProductsListView.as_view(), name="product_list"),
    path("add/", AddProductView.as_view(), name="add_product"),
    #path("products/", ProfileView.as_view(), name="profile_view"),
    #path("products/add", CompanyEditView.as_view(), name='company_edit')
]