from django import forms
from .models import Product
from .models import ProductCategory


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ["name", "category", "default_price", "price_currency", "internal_id"]

    def clean_name(self):
        try:
            name = self.cleaned_data['name']
        except KeyError:
            raise forms.ValidationError("Product name cannot be blank")
        return name

    def clean_category(self):
        try:
            category = self.cleaned_data['category']
        except KeyError:
            raise forms.ValidationError("Category cannot be blank")
        db_category = category
        if not db_category.id:
            raise forms.ValidationError("Category does not exist")
        return category

    def clean_default_price(self):
        try:
            default_price = self.cleaned_data['default_price']
        except KeyError:
            return None

        if default_price < 0:
            raise forms.ValidationError("Default price cannot be lower than 0!")
        return default_price

    def clean_price_currency(self):
        try:
            price_currency = self.cleaned_data['price_currency']
        except KeyError:
            raise forms.ValidationError("Currency for default price is mandatory")
        return price_currency

    def save(self, *args, **kwargs):
        self.instance.company = kwargs.pop('company')
        super(ProductForm, self).save(*args, **kwargs)


class ProductCategoryForm(forms.ModelForm):
    class Meta:
        model = ProductCategory
        fields = ['name', 'default_tax_rate']

    def clean_default_tax_rate(self):
        try:
            tax_rate = self.cleaned_data['default_tax_rate']
        except KeyError:
            raise forms.ValidationError("Default tax rate is mandatory")

        try:
            tax_rate = float(tax_rate)
        except:
            raise forms.ValidationError("Tax rate must be a number")

        if tax_rate < 0:
            raise forms.ValidationError("Tax rate cannot be negative!")
        return tax_rate

    def save(self, *args, **kwargs):
        self.instance.company = kwargs.pop('company')
        super(ProductCategoryForm, self).save(*args, **kwargs)

