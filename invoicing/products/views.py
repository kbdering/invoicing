from django.shortcuts import render
from django.views.generic import View
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from .models import ProductCategory, Product
from django.shortcuts import get_object_or_404, Http404, redirect
from .forms import ProductCategoryForm, ProductForm
from django.contrib import messages
from django.db import transaction
from django.views.decorators.cache import cache_page
from core.cache.decorators import cache_per_param


class ProductsListView(View):
    @method_decorator(login_required)
    def get(self, request):
        categories = ProductCategory.objects.select_related().filter(company=request.user.profile.company)
        return render(request, template_name="product/list.html", context={'categories': categories})

class AddProductView(View):
    @method_decorator(login_required)
    @method_decorator(cache_page(60 * 60))
    @method_decorator(cache_per_param(cache_per_user=True, param="id", prefix="add_product"))
    def get(self, request):
        form = ProductForm()
        form.fields['category'].queryset = ProductCategory.objects.filter(company=request.user.profile.company)
        return render(request, template_name="product/add.html", context={'form': form})

    @method_decorator(login_required)
    def post(self, request):
        form = ProductForm(request.POST)
        if form.is_valid():
            category = form.cleaned_data['category']
            if category.company != request.user.profile.company:
                raise Http404("The category does not belong to your company")

            with transaction.atomic():
                sid = transaction.savepoint()
                form.save(company=request.user.profile.company)

        else:
            return self.fail_request(request, form)

        messages.add_message(request, messages.SUCCESS, "Product added succesfully!")
        return redirect('product_list')

    def fail_request(self, request, form, exception=None):
        if Exception is not None:
            messages.add_message(request, messages.ERROR, exception)
        for error in form.errors:
            messages.add_message(request, messages.ERROR, error)
        return render(request, template_name='product/add_category.html', context={'form': form}, status=404)


class ProductCategoryView(View):
    @method_decorator(login_required)
    def get(self, request, id):
        product_category = get_object_or_404(ProductCategory, id=id)
        if product_category.company != request.user.profile.company:
            raise Http404
        return render(request, template_name="product/category.html",
                      context={'category': product_category})


class ProductCategoryEditView(View):
    @method_decorator(login_required)
    def get(self, request):
        try:
            id = request.GET['id']
            product_category = get_object_or_404(ProductCategory, pk=id)
            if product_category.company != request.user.profile.company:
                raise Http404
        except KeyError:
            product_category = None

        form = ProductCategoryForm(instance=product_category)
        return render(request, template_name='product/add_category.html', context={'form' : form})
    @method_decorator(login_required)
    def post(self, request):
        form = ProductCategoryForm(request.POST)
        if form.is_valid():
            try:
                id = request.GET['id']
                if id:
                    category = get_object_or_404(ProductCategory, pk=id)
                    if category.company != request.user.profile.company:
                        raise Http404

            except KeyError:
                category = False
            if category:
                category.update()
            else:
                with transaction.atomic():
                    try:
                        sid = transaction.savepoint()
                        form.save(company=request.user.profile.company, commit=True)
                        transaction.savepoint_commit(sid)
                    except Exception as e:
                        transaction.rollback(sid)
                        return self.fail_request(request, form, exception=e)
                messages.add_message(request,messages.SUCCESS, "Category added succesfully!" )
                return redirect('product_list')
        else:
            return self.fail_request(request, form)

    def fail_request(self, request, form, exception=None):
        if Exception is not None:
            messages.add_message(request, messages.ERROR, exception)
        for error in form.errors:
            messages.add_message(request, messages.ERROR, error)
        return render(request, template_name='product/add_category.html', context={'form': form}, status=404)
