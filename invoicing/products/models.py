#from django_mysql import models as mysqlmodels
from django.db import models
from company.models import Company
from django.contrib.auth.models import User
from financial.models import Currency
# Create your models here.


class ProductCategory(models.Model):
    name = models.CharField(max_length=80)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    default_tax_rate = models.FloatField()

    class Meta:
        models.Index(fields=["name",])
        ordering = ['name']

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=150)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    category = models.ForeignKey(ProductCategory, on_delete=models.CASCADE)
    default_price = models.FloatField()
    price_currency = models.ForeignKey(Currency, on_delete=models.CASCADE)
    internal_id = models.IntegerField(null=True)

    class Meta:
        models.Index(fields=["name",])
        ordering = ['-id']

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name
