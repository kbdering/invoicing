from django.shortcuts import render
from django.views import View
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from .models import Company, CompanyRelationship
from .forms import CustomerForm, AddressForm
from django.db import IntegrityError
from django.contrib import messages
from django.shortcuts import redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.cache import cache
from geographical.models import Country
from django.shortcuts import get_object_or_404, reverse
from django.http import HttpResponseForbidden
# Create your views here.

class CustomersView(View):
    @method_decorator(login_required)
    def get(self, request):
        customers_list = request.user.profile.company.customers.all()
        paginator = Paginator(customers_list, 25)
        page = request.GET.get('page')
        customers = paginator.get_page(page)
        return render(request, "company/customers.html", context={'customers': customers})

class AddCustomerView(View):
    @method_decorator(login_required)
    def get(self, request):

        country_set = cache.get(Country.cache_key)
        if not country_set:
            country_set = Country.objects.all()
            cache.set(Country.cache_key, country_set)
        form = CustomerForm()
        form.fields['country'].initial = request.user.profile.company.country
        form.fields['country'].queryset= country_set
        return render(request, "company/customer_form.html", context={'form': form})

    @method_decorator(login_required)
    def post(self, request):
        form = CustomerForm(request.POST)
        if form.is_valid():
            try:
                cleaned_data = form.cleaned_data
                try:
                    c = Company(**cleaned_data)
                    c.save()
                except IntegrityError as e:
                    c = Company.objects.get(tax_id=cleaned_data['tax_id'])
                CompanyRelationship(supplier=request.user.profile.company, customer=c).save()
                messages.add_message(request, messages.SUCCESS, "Company %s added successfully!" % c.name)
                return redirect("customers")

            except IntegrityError as e:
                if "unique constraint" in e.message:
                    form.add_error("tax_id", "Customer with following Tax ID already exists" )
        else:
            for error in form.non_field_errors():
                messages.add_message(request, messages.WARNING, error)
            for field in form:
                for error in field.errors:
                    messages.add_message(request, messages.WARNING, error)

        return render(request, "company/customer_form.html", context={'form': form.as_p()})

class CompanyView(View):
    @method_decorator(login_required)
    def get(self, request, id):
        company = get_object_or_404(Company.objects.get(pk=id))
        if request.user.profile.company != company:
            return HttpResponseForbidden()


class ProfileView(View):
    @method_decorator(login_required)
    def get(self, request, id):
        user = get_object_or_404(User,pk=id)
        if request.user.id != id:
            if request.user.profile.company != user.profile.company:
                return HttpResponseForbidden("You don't have access to this profile")
        return render(request, "company/profile.html")


class CompanyEditView(View):
    @method_decorator(login_required)
    def get(self, request):
        if request.user.profile.company.owner != request.user:
            return HttpResponseForbidden("Only the owner can edit this view")

        headquarters = request.user.profile.company.headquarters
        if headquarters:
            form = AddressForm(instance=headquarters)
        else:
            form = AddressForm()

        return render(request, "company/edit.html", context={"form": form})

    def post(self, request):
        form = AddressForm(request.POST)
        if form.is_valid():
            company = request.user.profile.company
            headquarters = company.headquarters
            print(headquarters)

            if headquarters:
                form.save(headquarters)
            else:
                headquarters = form.save()
                company.headquarters = headquarters
                company.save()
            return render(request, "company/profile.html")

        else:
            for error in form.errors:
                messages.add_message(request, messages.WARNING, error)
        return render(request, "company/edit.html", context={"form": form})
