from django import forms
from .models import Company, StaticAddress


class CustomerForm(forms.ModelForm):
    class Meta:
        model = Company
        fields = ["name", "country", "tax_id"]


class AddressForm(forms.ModelForm):
    class Meta:
        model= StaticAddress
        exclude = []