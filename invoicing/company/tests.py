from django.test import TestCase
from .models import Company, StaticAddress
from django.contrib.auth.models import User

# Create your tests here.
class CompanyTest(TestCase):
    def setUp(self):
        user1 = User.objects.create_user(username="test_user", password="password")
        address = StaticAddress.objects.create(street_name1="Żródlana", street_name2="2c")
        Company.objects.create(owner=user1,headquarters=address, tax_id="43242342")

    def test_object_exists(self):
        company = Company.objects.get(tax_id="43242342")
        assert(company.headquarters != None)
        assert(company.tax_id != None)



