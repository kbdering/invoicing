from django.db import models
from rest_framework.authtoken.models import Token
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User
from geographical.models import Country
from financial.models import TaxIdentifierType
# Create your models here.


class Address(models.Model):
    street_name1 = models.CharField(max_length=75)
    street_name2 = models.CharField(max_length=75, blank=True ,null=True)
    zipcode= models.CharField(max_length=15)
    city = models.CharField(max_length=60)
    email = models.CharField(max_length=60,blank=True, null=True)
    phone_number = models.CharField(max_length=60, blank=True, null=True)


class StaticAddress(Address):
    pass


class TemporaryAddress(models.Model):
    street_name1 = models.CharField(max_length=75)
    street_name2 = models.CharField(max_length=75, blank=True ,null=True)
    zipcode= models.CharField(max_length=15)
    city = models.CharField(max_length=60)
    email = models.CharField(max_length=60,blank=True, null=True)
    phone_number = models.CharField(max_length=60, blank=True, null=True)


class Company(models.Model):

    name = models.CharField(max_length=150)
    owner = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    headquarters = models.ForeignKey(StaticAddress, blank=True, on_delete=models.SET_NULL, null=True)
    tax_id = models.CharField(unique=True, max_length=50)
    customers = models.ManyToManyField("self", symmetrical=False, through='CompanyRelationship', blank=True)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    class Meta:
        models.Index(fields=['name', 'tax_id'])
        unique_together= (("country", "tax_id"))
        ordering = ["-id"]


class Alias(models.Model):
    name = models.CharField(max_length=150)
    company = models.ForeignKey(Company, related_query_name="company_alias", related_name="company_alias", on_delete=models.CASCADE)
    naming_company = models.ForeignKey(Company, related_query_name="naming_company", related_name="naming_company", on_delete=models.CASCADE)

    class Meta:
        models.Index(fields=["name", ])
        ordering = ['-id']


class CompanyRelationship(models.Model):
    supplier = models.ForeignKey(Company, related_name="supplier", on_delete=models.CASCADE)
    customer = models.ForeignKey(Company, related_name="customer", on_delete=models.CASCADE)

    class Meta:
        unique_together = (("supplier", "customer"),)


class Branch(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    address = models.ForeignKey(StaticAddress, on_delete=models.PROTECT)


class Profile(models.Model):
    ADMIN = "ADMIN"
    OWNER = "OWN"
    USER = "USR"
    ROLE_CHOICES = (
        (ADMIN, "Admin"),
        (OWNER, "Owner"),
        (USER, "User")
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, blank=True, null=True)
    employment_date = models.DateField(verbose_name="Employment Date", blank=True, null=True)
    role = models.CharField(
        max_length=5,
        choices=ROLE_CHOICES,
        default=USER
    )


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


# creating token for every new user
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
