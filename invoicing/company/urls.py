from django.urls import path
from django.urls import include
from .views import CustomersView, AddCustomerView, ProfileView, CompanyEditView

urlpatterns = [
    path("customers/", CustomersView.as_view(), name="customers"),
    path("customers/add", AddCustomerView.as_view(), name="add_customer"),
    path("profile/<int:id>", ProfileView.as_view(), name="profile_view"),
    path("edit", CompanyEditView.as_view(), name='company_edit')
]