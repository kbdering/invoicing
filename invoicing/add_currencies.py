import csv
from invoicing.settings import DATABASES

for database, data in DATABASES.items():
    host = data['HOST']
    passwd = data['PASSWORD']
    user = data['USER']
    dbname = data['NAME']
    if data['ENGINE'] == 'django.db.backends.mysql':
        import MySQLdb
        db = MySQLdb.connect(host=host, user=user, passwd=passwd, db=dbname)
    elif data['ENGINE'] == 'django.db.backends.postgresql':
        import psycopg2
        db = psycopg2.connect(dbname=dbname, user=user, host=host, password=passwd)

    cursor = db.cursor()
    with open("currencies.txt") as f:
        added_currencies = []
        for row in f.readlines():
            try:
                country, currency, alpha3 = row.split("\t")
                alpha3 = alpha3.split("\n")[0]
                try:
                    cursor.execute("INSERT INTO geographical_country (name, name_abbreviation) VALUES (%s,  %s)",
                                   [country, country])
                    if currency not in added_currencies:
                        cursor.execute("INSERT INTO financial_currency (name, alpha3) \
                    VALUES (%s,%s)", [currency, alpha3])
                        added_currencies.append(currency)
                except Exception as e:
                    print(e)
                cursor.execute("SELECT id from financial_currency where alpha3 = %s", alpha3)
                currency_id = cursor.fetchone()

                cursor.execute("SELECT id from geographical_country where name=%s")
                try:
                    res = cursor.fetchone()
                    if res:
                        cursor.execute("INSERT INTO geographical_currencyrelationship (country_id, currency_id) VALUES (%s, %s)", [res[1], currency_id])
                except:
                    pass
            except Exception as e:
                print(e)
                cursor.execute("ROLLBACK;")
    db.commit()
