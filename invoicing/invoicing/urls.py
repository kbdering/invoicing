"""invoicing URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls import include
from .views import index


#delete on going live
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Pastebin API')
urlpatterns = [
    path('admin/', admin.site.urls),
    path('invoice/', include('invoice.urls')),
    path('', index, name='index'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('company/', include('company.urls')),
    url(r'^api-auth/', include('rest_framework.urls')),
    url(r'^docs/', schema_view),
    path('api/', include("api.urls")),
    path('products/', include("products.urls")),
    path('orders/', include("orders.urls"))
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)



#delete when going to prod, static files have to be served on media server, memcached


if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls))
    ] + urlpatterns

from django.conf.urls import handler400, handler403, handler404, handler500


handler400 = 'invoicing.views.bad_request'
handler403 = 'invoicing.views.permission_denied'
handler404 = 'invoicing.views.page_not_found'
handler500 = 'invoicing.views.server_error'

