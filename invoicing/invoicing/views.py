from django.shortcuts import render, redirect, reverse
from django.views.decorators.vary import vary_on_cookie
from django.views.decorators.cache import cache_page

# HTTP Error 400

def bad_request(request):
    return render(request, "400.html", status=400)

def permission_denied(request):
    return render(request, "401.html", status=401)


def page_not_found(request):
    return render(request, "404.html", status=404)

def server_error(request):
    return render(request, "500.html", status=500)
def permission_denied(request):
    return render(request, "403.html", status=404)


@cache_page(60 * 60 * 24)
@vary_on_cookie
def index(request):
    return render(request, 'invoicing/index.html')