from django.conf import settings
from django.template.loaders.app_directories import get_app_template_dirs

import os
settings.configure()
template_files = []
for template_dir in (settings.TEMPLATES[0]['DIRS']+ get_app_template_dirs):
    for dir, dirnames, filenames in os.walk(template_dir):
        for filename in filenames:
            template_files.append(os.path.join(dir, filename))

for template in template_files:
    print(template)