
from django.urls import path

from .views import InvoiceAcceptView, AddInvoiceView, EditInvoiceView, InvoiceListView, IssuedInvoicesListView


urlpatterns = [
    path("add", AddInvoiceView.as_view(), name="new_invoice"),
    path("edit/<int:id>", EditInvoiceView.as_view(), name="edit_invoice"),
    path("list/received", InvoiceListView.as_view(), name="invoices_received"),
    path("list/issued", IssuedInvoicesListView.as_view(), name="invoices_issued"),
    path("accept/<int:id>/", InvoiceAcceptView.as_view(), name="accept_invoice"),
]
