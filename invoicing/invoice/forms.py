from django import forms
from .models import Invoice, Row, TemporaryAddress
from django.forms.widgets import DateInput, NumberInput, TextInput
import datetime


class InvoiceForm(forms.ModelForm):

    def __init__(self, issuer, *args, **kwargs):
        super(InvoiceForm,self).__init__(*args,**kwargs)
        self.fields['currency'].initial = issuer.country.currencies.values_list('pk', flat=True)[:1].get()

    class Meta:
        model= Invoice
        exclude = ['items','issuer', 'created_by', 'updated_by', 'issuer_branch', 'update_date', 'recipient']
        widgets = {
            'due_date': DateInput(attrs={'type': 'date'}),
            'issue_date': DateInput(attrs={'type':'date', "value": datetime.datetime.now().date()} )
        }
        labels = {
            "internal_id" : "Invoice Nr",
            "tax_id" : "Tax ID"
        }

    def clean(self):
        cleaned_data = super().clean()
        due_date = cleaned_data['due_date']
        issue_date = cleaned_data['issue_date']
        if due_date < issue_date:
            self.add_error('due_date', "Due date cannot be before the issue date!")


class RowForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(RowForm, self).__init__(*args, **kwargs)
        self.fields['net_total'].required = False
        self.fields['gross_total'].required = False

    class Meta:
        model = Row
        exclude = ["index", "invoice"]
        widgets = {
            'quantity': NumberInput(attrs={'min': 0, "required" : True, 'onchange': "changeEventHandler(this)", "value":0, "step" : 1}),
            'tax_rate': NumberInput(attrs={'min': 0, 'max': 100, 'required': True, 'onchange': "changeEventHandler(this)", "class": "input-holder"}),
            'price': NumberInput(attrs={'min': 0, 'required': True, 'onchange': "changeEventHandler(this)"}),
            'name' : TextInput(attrs={'required' : True}),
            'net_total': TextInput(attrs={"readonly": True}),
            'gross_total': TextInput(attrs={"readonly": True})
        }

    def clean_quantity(self):
        try:
            quantity = self.cleaned_data['quantity']
        except:
            raise forms.ValidationError("Quantity cannot be blank")
        try:
            quantity = float(quantity)
        except:
            raise forms.ValidationError("This field must be a number")
        if quantity < 0:
            raise forms.ValidationError("Quantity cannot be lesser than zero!")
        return quantity

    def clean_tax_rate(self):
        try:
            tax_rate = self.cleaned_data['tax_rate']
        except:
            raise forms.ValidationError("The tax rate must be entered")
        try:
            float(tax_rate)
        except:
            raise forms.ValidationError("Tax rate must be a number")
        if tax_rate < 0 or tax_rate > 100:
            raise forms.ValidationError("Tax rate must be between 0 and 100%!")
        return tax_rate

    def clean_name(self):
        name= self.cleaned_data['name']
        if not name:
            raise forms.ValidationError("Name cannot be blank")
        return name

    def clean_price(self):
        try:
            price = self.cleaned_data['price']
        except:
            raise forms.ValidationError("Price must be not blank")
        try:
            float(price)
        except:
            raise forms.ValidationError("The price must be a number")

        if price < 0:
            raise forms.ValidationError("Price cannot be a negative value")
        return price

    def clean(self):
        self.cleaned_data = super().clean()
        self.calculate_totals(self.cleaned_data)

    def calculate_totals(self, cleaned_data):
        quantity = cleaned_data['quantity']
        price = cleaned_data['price']
        tax_rate = cleaned_data['tax_rate']
        net_total = quantity * price
        gross_total = net_total + net_total * tax_rate / 100
        self.cleaned_data['net_total'] = net_total
        self.cleaned_data['gross_total'] = gross_total


class EditInvoiceForm(InvoiceForm):
    class Meta(InvoiceForm.Meta):
        widgets= {
            "internal_id" : TextInput(attrs={"readonly": True, }),
            **InvoiceForm.Meta.widgets
        }


class AddressForm(forms.ModelForm):

    def save(self, invoice, commit=True):
        instance = super(AddressForm, self).save(commit=False)
        instance.invoice = invoice  # etc
        if commit:
            instance.save()
        return instance

    class Meta:
        model = TemporaryAddress
        exclude = ["invoice"]
        labels ={
        "street_name2": "Street Name 2",
        "street_name1": "Street Name 1",
            "zipcode" : "Zip-code",
            "email" : "e-mail"
        }


RowFormSet = forms.formset_factory(
    form=RowForm,
    extra=0,
    min_num=1
)
InvoiceFormSet = forms.inlineformset_factory(
    exclude=[],
    model=Row,
    parent_model=Invoice,
    extra=0,
    formset=RowForm,
    min_num=1,
)

