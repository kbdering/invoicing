from django.shortcuts import render
from django.views.generic import View
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required
from .models import  Invoice, Row, InvoiceStatus
from .forms import InvoiceForm, RowFormSet, EditInvoiceForm, AddressForm
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, Http404
from django.contrib import messages
from company.models import Company
from django.db.models import Q
from django.core.paginator import Paginator
from django.db import transaction, IntegrityError
from django.views.decorators.cache import cache_page
from core.cache.decorators import cache_per_param
from django.shortcuts import reverse


class AddInvoiceView(View):

    @method_decorator(login_required)
    @method_decorator(cache_page(60*60*10))
    @method_decorator(cache_per_param(cache_per_user=True, param="recipient", prefix="add_invoice"))
    def get(self, request):
        company = request.user.profile.company
        recipient=None
        if request.GET.get('recipient'):
            recipient_id = request.GET['recipient']
            if company.customers.filter(id__exact=recipient_id):
                recipient = get_object_or_404(Company, pk=recipient_id)
            else:
                return HttpResponse("The selected company is not in your customers list")

        form = InvoiceForm(company,initial={ 'recipient_name':recipient.name if recipient else None,
                                             'tax_id': recipient.tax_id if recipient else None})
        address = AddressForm(instance=recipient.headquarters if recipient else None)
        rows = RowFormSet()
        return render(request, "invoice/new_invoice.html", context={'form': form, 'formset' : rows, 'address_form' : address})


    @method_decorator(login_required)
    def post(self, request):
        form = InvoiceForm(request.user.profile.company, request.POST)

        rows = RowFormSet(request.POST)
        address = AddressForm(request.POST)
        if form.is_valid() and all([row.is_valid() for row in rows]):
            new_invoice = form.save(commit=False)
            row_objects = []

            for row in rows:
                row_objects.append(row.cleaned_data)
            try:
                with transaction.atomic():
                    sid = transaction.savepoint()
                    try:
                        new_invoice.save(row_objects, request.user)
                    except IntegrityError as e:
                        transaction.savepoint_rollback(sid)
                        raise e



                    if address.is_valid():
                        address.save(new_invoice)
                        transaction.savepoint_commit(sid)
                        messages.add_message(request, messages.SUCCESS, "Invoice Created!")
                    else:
                        #print(address.errors)
                        transaction.savepoint_rollback(sid)
                        return self.fail_request(request,form, rows, address)
                    return render(request, "invoice/new_invoice.html",
                                  context={'form': form,'formset': rows, 'address_form': address})
            except IntegrityError:
                messages.add_message(request, messages.WARNING, "The invoice with the following id already exists!")
                return self.fail_request(request, form, rows, address)
        else:
            return self.fail_request(request,form,rows,address)

    def fail_request(self, request, form, rows, address):
            messages.add_message(request, messages.WARNING, "Please fix the errors")

            for error in form.non_field_errors():
                messages.add_message(request, messages.WARNING, error)
            for error in address.non_field_errors():
                messages.add_message(request, messages.WARNING, error)

            for row in rows:
                for error in row.non_field_errors():
                    messages.add_message(request, messages.WARNING, error)

            return render(request, "invoice/new_invoice.html",
                          context={'form': form, 'formset': rows, 'address_form': address}, status=401)

class EditInvoiceView(View):
    @method_decorator(login_required)
    def get(self, request, id):
        company = request.user.profile.company
        invoice= get_object_or_404(Invoice, pk=id)
        if company not in [invoice.issuer, invoice.recipient]:
            return HttpResponse("Unauthorized", status=401)
        form = EditInvoiceForm(request.user.profile.company, instance=invoice)
        rows = RowFormSet(initial=invoice.items)
        return render(request, "invoice/new_invoice.html", context={'form': form, 'formset': rows})




class InvoiceListView(View):
    @method_decorator(login_required)
    def get(self, request):
        invoices_list = Invoice.objects.prefetch_related('issuer').filter(Q(recipient=request.user.profile.company) |
                                               Q(tax_id=request.user.profile.company.tax_id) &
                                               ~Q(invoicestatus__status="Accepted"))
        paginator = Paginator(invoices_list, 25)
        page = request.GET.get('page')
        invoices = paginator.get_page(page)
        return render(request, "invoice/list.html", context={'invoices':invoices })

class InvoiceAcceptView(View):
    @method_decorator(login_required)
    def post(self, request, id):
        invoice = get_object_or_404(Invoice, pk=id)
        if invoice.tax_id == request.user.profile.company.tax_id:
            with transaction.atomic():
                status =InvoiceStatus(invoice=invoice, status="Accepted")
                status.save()
                return HttpResponseRedirect(reverse('invoices_received'))
        else:
            raise Http404

    def get(self,request, id):
        raise Http404




class IssuedInvoicesListView(View):
    @method_decorator(login_required)
    def get(self, request):
        company=request.user.profile.company
        company_id = company.id
        invoices_list = Invoice.objects.select_related().filter(Q(issuer__id=company_id))
        paginator = Paginator(invoices_list, 25)
        page = request.GET.get('page')
        invoices = paginator.get_page(page)
        return render(request, "invoice/issued_list.html", context={'invoices':invoices })

