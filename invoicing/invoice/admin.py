from django.contrib import admin

# Register your models here.
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

try:
    for user in User.objects.all():
        Token.objects.get_or_create(user=user)
except:
    pass


from rest_framework.authtoken.admin import TokenAdmin

TokenAdmin.raw_id_fields = ('user',)