from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from company.models import Address
from financial.models import Currency

# Create your models here.
class Invoice(models.Model):
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL,null=True, related_name='created_by')
    updated_by = models.ForeignKey(User, on_delete=models.SET_NULL,null=True, related_name='updated_by')
    internal_id = models.CharField(max_length=100)
    issuer = models.ForeignKey('company.Company',  on_delete=models.SET_NULL,null=True, related_name="issuers")
    issuer_branch = models.ForeignKey('company.Branch', on_delete=models.SET_NULL,null=True)
    items = JSONField()
    recipient = models.ForeignKey('company.Company', on_delete=models.SET_NULL,null=True,  related_name="recipient")
    recipient_name = models.CharField(max_length=150)
    created_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(null=True)
    issue_date = models.DateField()
    due_date = models.DateField()
    currency = models.ForeignKey(Currency,  on_delete=models.SET_NULL,null=True)
    tax_id = models.CharField(max_length=150)
    class Meta:
        unique_together = (('issuer', 'internal_id'),)
        index_together = (('issuer', 'internal_id'),)
    def save(self,rows, user, *args, **kwargs):
        if self.id:
            self.update_date = timezone.now()
            self.updated_by = user
            self.items = rows
        else:
            self.created_by = user
            self.created_date = timezone.now()
            self.issuer = user.profile.company
            self.items = rows

        return super(Invoice, self).save(*args, **kwargs)

    def get_rows(self):
        rows = []
        for row in self.items:
            rows.append(Row(row))
        return rows

    class Meta:
        ordering = ['-id']

class Row(models.Model):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
    name = models.CharField(max_length=150)
    quantity = models.FloatField()
    price = models.FloatField()
    tax_rate = models.FloatField()
    net_total = models.FloatField()
    gross_total = models.FloatField()
    invoice = models.ForeignKey(Invoice, on_delete=models.CASCADE)
    class Meta:
        abstract = True

    def to_json(self):
        return {
            "name" : self.name,
            "quantity": self.quantity,
            "price" : self.price,
            "tax_rate": self.tax_rate
        }

class TemporaryAddress(Address):
    invoice = models.OneToOneField(Invoice, on_delete=models.CASCADE)


class InvoiceStatus(models.Model):
    PENDING = "PENDING"
    ACCEPTED = "ACCEPTED"
    REJECTED = "REJECTED"

    STATUS_CHOICES = (
        (PENDING, "Pending"),
        (ACCEPTED, "Accepted"),
        (REJECTED, "Rejected")
    )
    invoice = models.OneToOneField(Invoice, on_delete=models.CASCADE)
    status = models.CharField(choices=STATUS_CHOICES, max_length=10)
