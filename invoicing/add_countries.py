import csv


from invoicing.settings import DATABASES

for database, data in DATABASES.items():
    host = data['HOST']

    passwd = data['PASSWORD']
    user = data['USER']
    dbname = data['NAME']
    if data['ENGINE'] == 'django.db.backends.mysql':
        import _mysql
        import MySQLdb
        db = MySQLdb.connect(host=host, user=user, passwd=passwd, db=dbname)
    elif data['ENGINE'] == 'django.db.backends.postgresql':
        import psycopg2
        db = psycopg2.connect(dbname=dbname, user=user, host=host, password=passwd)
    cursor = db.cursor()
    with open("countries.txt") as f:
        added_countries = []
        for row in f.readlines():
            print(row)
            try:
                id, name, population, area, density  = row.split("\t")
                if name not in added_countries:
                    cursor.execute("INSERT INTO geographical_country (name) \
                VALUES (%s)", [name])
                    added_countries.append(name)
            except Exception as e:
                print(e)
    db.commit()
